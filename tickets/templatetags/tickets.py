from decimal import Decimal
from typing import Dict
import json
import logging

from django import template
from django.template import Template
from django.utils.safestring import mark_safe
import django_editorjs_parser

import tickets.queries

logger = logging.getLogger(__name__)
editorjs_parser = django_editorjs_parser.parser.EditorJSParser()
register = template.Library()

currency_symbol = {
    'EUR': '€',
    'USD': '$',
}
tpl_saleor_product_image_url = Template('{{ order.lines.0.variant.product.thumbnail.url }}')
tpl_stripe_product_image_url = Template('{{ order.line_items.data.0.price.product.images.0 }}')


@register.filter
def format_saleor_money(value: Dict[str, str]) -> str:
    """Format money dict coming from Saleor API."""
    return f'{currency_symbol[value["currency"]]}\u00a0{value["amount"]:.2f}'


@register.simple_tag
def format_cents(cents: int, currency: str):
    """Format price amount in cents.

    Used to display amounts comingfrom Stripe API.
    """
    currency = currency.upper()
    whole = cents // 100
    cents = cents % 100
    decimal_amount = Decimal(f'{whole}.{cents:02}')
    return f'{currency_symbol[currency]}\u00a0{decimal_amount:.2f}'


@register.filter
def editorjs(editorjs_object: Dict[str, str]) -> str:
    """Render Editor.JS object as "safe" HTML."""
    try:
        return mark_safe(editorjs_parser.parse(json.loads(editorjs_object)))
    except Exception:
        logger.exception('Unable to render an Editor.JS object')
        return ''


@register.simple_tag(takes_context=True)
def absolute_url(context, path: str) -> str:
    """Return an absolute URL of a given path."""
    request = context.get('request')
    if not request:
        return ''
    return request.build_absolute_uri(path)


@register.simple_tag()
def is_claimed_by(ticket, user) -> str:
    """Return True if given user is claimed the given ticket."""
    return ticket.is_claimed_by(user.pk)


@register.simple_tag()
def is_attending_edition(user, edition) -> str:
    """Return True if given user is has a valid ticket for given edition."""
    return tickets.queries.is_attending_edition(user, edition)


@register.simple_tag(takes_context=True)
def get_product_image_url(context) -> str:
    """Return URL of the ticket's image as shown at the payment back-end."""
    ticket = context['ticket']
    if ticket.is_saleor:
        return tpl_saleor_product_image_url.render(context)
    if ticket.is_stripe:
        return tpl_stripe_product_image_url.render(context)
    return ''

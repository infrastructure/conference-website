---
gitea: none
include_toc: true
---

# Blender Conference

## Requirements

* Python 3.10
* [venv](https://docs.python.org/3.10/library/venv.html)

## Development

### Setup

Create and activate a virtual environment using your favourite method, e.g.

    python3.10 -m venv .venv
    source .venv/bin/activate

Install required packages:

    pip install -r requirements_dev.txt

Make a copy of example `.env`:

    cp .env.example .env

Initialise submodules included into this repo:

    git submodule update --init

Create the database tables:

    ./manage.py migrate

Create a superuser account to be able to login and access `/admin/`:

    ./manage.py createsuperuser

Update `/etc/hosts` to point to `conference.local`, e.g.:

    127.0.0.1	conference.local

Run development server

    ./manage.py runserver 8009

Now http://conference.local:8009 should be ready to work with and it should be possible to
log into http://conference.local:8009/admin/ with the username and password you've used
during `./manage.py createsuperuser`.

### Blender ID

For development, Blender ID's code contains a fixture with an OAuth app
that should work without any changes to default configuration.
To load this fixture, go to your development Blender ID and run the following:

    ./manage.py loaddata blender_conference_devserver

### Create system user

Conference uses a special system user to log various changes via admin `LogEntry`.

In order for this to work locally, `SYSTEM_USER_ID` must be set in the `.env`.
The following will create a user and print out its ID, which you can then copy-paste into your `.env`:

    echo "from django.contrib.auth import get_user_model; User = get_user_model(); print(User.objects.create_user('system', 'system@blender.org', 'password').id)" | ./manage.py shell

## Running tests

Tests can be run by issuing `./test.sh` in the repository root directory.


## Update BWA (`assets_shared/`)

For updating the base styling provided by [Blender Web Assets](https://developer.blender.org/diffusion/BWA/), navigate to `assets_shared/` and pull latest changes:

    cd assets_shared/
    git checkout main && git pull
    cd ..

**N.B.:** because project's static assets depend on BWA SASS modules, and "compiling" those can fail due to, e.g., incorrect font paths or missing files, always check that `collectstatic` works before committing this kind of update:

    ./manage.py collectstatic  --noinput

After making sure everything looks as expected, commit the updated submodule reference:

    git add assets_shared
    # add and commit the rest of the changes


## Panel setup

* The panel is available at `/<edition>/panel`



## Media and Static Files configuration

By default, we use Django's default static files configuration. However, in case of deployment to
platforms that do not support serving static files, it is possible to configure S3 compatible
storage for media, and enable WhiteNoise to serve static assets.

This can be done by adding declaring the following env variables:

```
AWS_STORAGE_BUCKET_NAME=''
AWS_S3_REGION_NAME=''
AWS_S3_ENDPOINT_URL=''
AWS_ACCESS_KEY_ID=''
AWS_SECRET_ACCESS_KEY=''

MEDIA_ROOT='media'
MEDIA_URL='${AWS_S3_ENDPOINT_URL}/'
STORAGE_DEFAULT='storages.backends.s3.S3Storage'

USE_WHITENOISE=True
```

## Custom Looks

Use the `HAS_OVERRIDES_APP=True` setting to register an optional 'overrides' app which whill
override templates and static (useful to fully customize the look of the website).

# Deploy

See [playbooks](playbooks#deploy).

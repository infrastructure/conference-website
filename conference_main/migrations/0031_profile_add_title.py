# Generated by Django 2.2.6 on 2019-10-16 14:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [('conference_main', '0030_flatfile')]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='title',
            field=models.CharField(
                blank=True, default='', help_text='3D Artist, CEO, etc.', max_length=50
            ),
        )
    ]

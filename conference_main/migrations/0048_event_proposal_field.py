# Generated by Django 3.2.13 on 2022-09-20 17:15

from django.db import migrations, models


def forwards_func(apps, schema_editor):
    event_model = apps.get_model("conference_main", "Event")
    db_alias = schema_editor.connection.alias

    for event in event_model.objects.using(db_alias).all():
        event.proposal = event.description
        event.save()


class Migration(migrations.Migration):

    dependencies = [
        ('conference_main', '0047_alter_day_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='proposal',
            field=models.TextField(default='-'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='event',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.RunPython(forwards_func, migrations.RunPython.noop),
    ]

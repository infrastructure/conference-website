from typing import Dict, Any

import datetime
from dateutil import tz
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from django.views.generic.base import View, TemplateResponseMixin

from conference_main import models


class TimeOverride:
    day: int = None
    hour: int = None

    def __init__(self):
        pass

    def reset(self):
        self.day = None
        self.hour = None

    def has_override(self):
        return (self.day is not None) or (self.hour is not None)


class DateTimeOverrideMixin(View):
    # Initialize empty override.
    #
    # NOTE: It is not possible to rely on the timezone.now() being up-to-date here
    # since this code is not run on the dynamic pages update. The current time is
    # only available in the setup().
    timestamp_overrides = TimeOverride()

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)

        # Reset the override.
        # Without this reloading the page is not enough since the class initialization
        # does not happen.
        self.timestamp_overrides.reset()

        day_override = request.GET.get('day', '')
        if day_override:
            self.timestamp_overrides.day = int(day_override)

        hour_override = request.GET.get('hour', '')
        if hour_override:
            self.timestamp_overrides.hour = int(hour_override)

    def get_current_amsterdam_time_with_override(self):
        current_timestamp = datetime.datetime.now(tz=tz.gettz('Europe/Amsterdam'))

        # Define to debug
        if self.timestamp_overrides.day is not None:
            current_timestamp = current_timestamp.replace(day=self.timestamp_overrides.day)

        if self.timestamp_overrides.hour is not None:
            # When hour override is used additionally override the minute and the second,
            # otherwise there will be always a confusion when events scheduled for that hour
            # disappear due to non-zero minute at the original °now° time.
            #
            # For example, when he local time is 10:47, the hour override is 9, and the event
            # is from 9:00 to 9:30 without round-to-the-beginning-of-the-hour the event will
            # disappear from the schedule.
            #
            # Always set microseconds to zero.
            # In a normal operation we do not need this precision, and with overrides we want
            # to round down to the beginning of the hour.
            current_timestamp = current_timestamp.replace(
                hour=self.timestamp_overrides.hour,
                minute=0,
                second=0,
                microsecond=0,
            )
        return current_timestamp


class PanelView(DateTimeOverrideMixin, TemplateResponseMixin, View):
    template_name = 'conference_main/panel/panel.pug'

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:
        return self.render_to_response({})


class PanelLocationView(DateTimeOverrideMixin, TemplateView):
    template_name = 'conference_main/panel/panel_location.pug'

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['edition'] = get_object_or_404(models.Edition, path=kwargs['edition_path'])
        context['location'] = get_object_or_404(models.Location, slug=kwargs['location_slug'])

        current_timestamp = self.get_current_amsterdam_time_with_override()

        context['date'] = current_timestamp

        # Allow to display only content after a certain hour
        after = self.request.GET.get('after', 0)
        try:
            after = int(after)
        except TypeError:
            after = 0
        after = datetime.time(after)

        context['events'] = (
            models.Event.objects.filter(
                status='accepted',
                location=context['location'],
                edition=context['edition'],
                day__date=current_timestamp,
                time__gt=after,
            )
            .prefetch_related('tags')
            .prefetch_related('speakers')
        )
        return context


class EventsView(DateTimeOverrideMixin, TemplateResponseMixin, View):
    template_name = 'conference_main/panel/events.pug'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: models.Edition = models.Edition.objects.get(path=kwargs['edition_path'])

    def get(self, request: HttpRequest, *args: str, **kwargs: str) -> HttpResponse:
        current_timestamp = self.get_current_amsterdam_time_with_override()

        assert (
            current_timestamp.tzinfo is not None
            and current_timestamp.tzinfo.utcoffset(current_timestamp) is not None
        ), 'current_timestamp is not timezone aware.'

        current_events = models.Event.objects.raw(
            '''
            SELECT
                conference_main_event.*
            FROM conference_main_event
                INNER JOIN conference_main_day
                    ON conference_main_event.day_id = conference_main_day.id
            WHERE
                conference_main_event.edition_id = %(edition_id)s
                AND conference_main_event.status = 'accepted'
                AND %(current_timestamp)s BETWEEN
                        ((conference_main_day.date + conference_main_event.time) AT TIME ZONE 'Europe/Amsterdam')
                    AND
                        ((conference_main_day.date + conference_main_event.time + make_interval(mins => conference_main_event.duration_minutes)) AT TIME ZONE 'Europe/Amsterdam')
            ORDER BY
                (conference_main_day.date + conference_main_event.time) AT TIME ZONE 'Europe/Amsterdam'
            ;
        ''',
            {'edition_id': self.edition.id, 'current_timestamp': current_timestamp},
        )

        for e in current_events:
            e.is_ongoing = True

        upcoming_events = models.Event.objects.raw(
            '''
            SELECT DISTINCT ON (conference_main_event.location_id)
                conference_main_event.*
            FROM conference_main_event
                INNER JOIN conference_main_day
                    ON conference_main_event.day_id = conference_main_day.id
            WHERE
                conference_main_event.edition_id = %(edition_id)s
                AND conference_main_event.status = 'accepted'
                AND %(current_timestamp)s <
                    ((conference_main_day.date + conference_main_event.time) AT TIME ZONE 'Europe/Amsterdam')
                AND (conference_main_day.date AT TIME ZONE 'Europe/Amsterdam') < %(current_timestamp)s
            ORDER BY
                conference_main_event.location_id,
                ((conference_main_day.date + conference_main_event.time) AT TIME ZONE 'Europe/Amsterdam')
            ;
        ''',
            {'edition_id': self.edition.id, 'current_timestamp': current_timestamp},
        )

        for e in upcoming_events:
            e.is_ongoing = False

        events = list(current_events) + list(upcoming_events)
        events.sort(key=lambda x: x.time)

        return self.render_to_response(
            {'events': events, 'has_time_override': self.timestamp_overrides.has_override()}
        )

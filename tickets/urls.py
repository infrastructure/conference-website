from django.urls import path

from tickets.views import (
    checkout,
    invoice,
    tickets,
    webhooks_saleor,
    webhooks_stripe,
)


app_name = 'tickets'
urlpatterns = [
    path('tickets/overview/', checkout.ProductsTableView.as_view(), name='products-table'),
    path('tickets/book/<str:slug>/', checkout.StripeTicketBuyView.as_view(), name='stripe-buy'),
    path('tickets/checkout/done/', checkout.StripeCheckoutDone.as_view(), name='stripe-done'),
    path('tickets/<str:sku>/', checkout.TicketBuyView.as_view(), name='buy'),
    path(
        'tickets/checkout/<str:checkout_token>/', checkout.CheckoutView.as_view(), name='checkout'
    ),
    path('tickets/claim/<str:ticket_token>/', tickets.TicketClaimView.as_view(), name='claim'),
    path('<str:edition_path>/tickets/stats/', tickets.tickets_stats, name='stats'),
    path('account/ticket/<str:ticket_token>/', tickets.TicketDetailView.as_view(), name='detail'),
    path('account/tickets/', tickets.TicketsListView.as_view(), name='list'),
    path('webhooks/tickets/order/', webhooks_saleor.OrderUpdated.as_view(), name='webhook-saleor'),
    path('webhooks/stripe/', webhooks_stripe.StripeWebhookView.as_view(), name='webhook-stripe'),
    path('account/invoice-<str:ticket_token>.pdf', invoice.PDFView.as_view(), name='invoice-pdf'),
]

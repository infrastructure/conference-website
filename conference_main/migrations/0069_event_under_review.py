# Generated by Django 3.2.13 on 2023-08-01 13:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conference_main', '0068_edition_location_coordinates'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='status',
            field=models.CharField(choices=[('submitted', 'Submitted'), ('accepted', 'Accepted'), ('rejected', 'Rejected'), ('cancelled', 'Cancelled'), ('under_review', 'Under Review')], default='submitted', max_length=20),
        ),
    ]

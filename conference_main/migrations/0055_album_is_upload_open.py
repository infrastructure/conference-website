# Generated by Django 3.2.13 on 2022-09-22 16:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conference_main', '0054_photo_hash'),
    ]

    operations = [
        migrations.AddField(
            model_name='album',
            name='is_upload_open',
            field=models.BooleanField(blank=True, default=False, help_text="If set, a link to this album will be shown in the edition's header and attendees will be able to upload photos to this album."),
        ),
    ]

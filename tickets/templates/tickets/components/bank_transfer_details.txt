{% if order.payment_intent.next_action.display_bank_transfer_instructions %}{% with instructions=order.payment_intent.next_action.display_bank_transfer_instructions %}{% with bank=instructions.financial_addresses.0 %}{{ bank.iban.account_holder_name }}
BIC: {{ bank.iban.bic }}
IBAN: {{ bank.iban.iban }}{% endwith %}{% endwith %}{% else %}
Blender Institute B.V.
Bank: ING Bank
IBAN: NL22 INGB 0005296212
BIC/Swift code: INGB NL2A{% endif %}

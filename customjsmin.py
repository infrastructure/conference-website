from pipeline.compressors import CompressorBase


class JSMinCompressor(CompressorBase):
    """
    Passes additional parameters to jsmin, to help support backticks.

    See https://github.com/tikitu/jsmin/issues/37
    """

    def compress_js(self, js):
        from jsmin import jsmin

        return jsmin(js, quote_chars="'\"`")

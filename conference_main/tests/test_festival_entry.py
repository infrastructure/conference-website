from typing import Optional

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core import mail
from django.test import TestCase
from django.urls import reverse

from conference_main.models import Edition, FestivalEntry, MessageExchange, SiteSettings
from conference_main.util import login_url_with_redirect


class FestivalEntryTest(TestCase):
    def setUp(self) -> None:
        self.owner_user = User.objects.create_user(
            username='owner_user',
            email='test@test.nl',
            password='12345',
            is_superuser=False,
            is_staff=False,
        )
        self.unrelated_user = User.objects.create_user(username='unrelated_user', password='12345')
        self.staff_user = User.objects.create_user(
            username='staff_user', password='12345', is_superuser=True, is_staff=True
        )

        self.edition = Edition.objects.create(
            year=2019, location='Nowhere', path='2019', title='Blender Conference 2019'
        )
        self.site_settings = SiteSettings.objects.create(
            site=Site.objects.get_current(), current_edition=self.edition
        )

    def _create_festival_entry(self) -> FestivalEntry:
        return FestivalEntry.objects.create(
            user=self.owner_user,
            title='How to train your dragon сенсерит',
            description='Лорем ипсум долор сит амет, ех хас синт вениам сенсерит, '
            'дуо ут юсто игнота аппетере. Ад пхаедрум омиттантур при, '
            'ид вел аццусамус маиестатис адверсариум.',
            credits='Mr. Director амет',
            website='http://www.google.nl/',
            video_link='http://www.google.nl',
            edition=Edition.objects.latest('year'),
        )

    def test_creation(self):
        festival_entry = self._create_festival_entry()

        self.assertEqual('submitted', festival_entry.status)
        self.assertEqual('', festival_entry.category)

    def test_submit_get(self):
        url = reverse('festival_entry_submit', kwargs={'edition_path': self.edition.path})

        self.edition.is_archived = True
        self.edition.save()

        # Test a user cannot view the submission form if the Edition is archived.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.festival_submissions_open = False
        self.edition.save()

        # Test a user can view the submission form even if the submissions are closed.
        # This is because from past experience we know that being able to send users
        # who were too late with submissions a link to the submission form is really handy.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        # Test an anonymous user cannot view the submission form.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_submit_post(self):
        title = 'How to train your dragon сенсерит'
        description = 'Лорем ипсум долор сит амет, ех хас синт вениам сенсерит,'
        credits = 'Mr. Director амет'
        website = 'http://www.google.nl'
        video_link = 'http://www.google.nl/video'

        url = reverse('festival_entry_submit', kwargs={'edition_path': self.edition.path})
        data = {
            'title': title,
            'description': description,
            'credits': credits,
            'website': website,
            'video_link': video_link,
        }

        self.edition.is_archived = True
        self.edition.save()

        # Test a user cannot submit presentations if the Edition is archived.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.festival_submissions_open = False
        self.edition.save()

        # Test a user can submit a presentation even if the submissions are closed.
        # This is because from past experience we know that being able to send users
        # who were too late with submissions a link to the submission form is really handy.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse('account_festival_entries_list'), response.url)

        festival_entry = FestivalEntry.objects.get(user=self.owner_user)
        self.assertEqual(title, festival_entry.title)
        self.assertEqual(description, festival_entry.description)
        self.assertEqual(credits, festival_entry.credits)
        self.assertEqual(website, festival_entry.website)
        self.assertEqual(video_link, festival_entry.video_link)

        # Test an anonymous user cannot submit entries.
        self.client.logout()
        response = self.client.post(url, data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_festival_entry_detail(self):
        festival_entry = self._create_festival_entry()
        url = reverse(
            'festival_entry_detail',
            kwargs={'edition_path': festival_entry.edition.path, 'pk': festival_entry.pk},
        )

        # Test the owner and staff can view the entry.
        for user in (self.owner_user, self.staff_user):
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

        # Test an unrelated user cannot view a non-accepted entry.
        self.client.force_login(self.unrelated_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        # Test an anonymous user cannot view a non-accepted entry.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

        # Tests for accepted entries.
        for status in ('accepted', 'winner'):
            festival_entry.status = status
            festival_entry.save()

            # Test an unrelated user can view an accepted entry.
            self.client.force_login(self.unrelated_user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

            # Test an anonymous user can view an accepted entry.
            self.client.logout()
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

    def test_festival_entry_update_get(self):
        festival_entry = self._create_festival_entry()

        url = reverse(
            'festival_entry_update',
            kwargs={'edition_path': festival_entry.edition.path, 'pk': festival_entry.pk},
        )

        self.edition.is_archived = True
        self.edition.save()

        # Test the owner cannot view the update form if `Edition.is_archived == True`
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.save()

        # Test the owner and staff can view the update form.
        for user in (self.owner_user, self.staff_user):
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)

        # Test an unrelated user cannot view the update form.
        self.client.force_login(self.unrelated_user)
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        # Test an anonymous user cannot view the update form.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

    def test_festival_entry_update_post(self):
        festival_entry = self._create_festival_entry()

        title = 'How to train your dragon сенсерит aaa'
        description = 'Лорем ипсум долор сит амет, ех хас синт вениам сенсерит, aaa'
        credits = 'Mr. Director амет aaa'
        website = 'http://www.google.nl/aaa'
        video_link = 'http://www.google.nl/videoaaa'

        url = reverse(
            'festival_entry_update',
            kwargs={'edition_path': festival_entry.edition.path, 'pk': festival_entry.pk},
        )
        data = {
            'title': title,
            'description': description,
            'credits': credits,
            'website': website,
            'video_link': video_link,
        }

        detail_url = reverse(
            'festival_entry_detail',
            kwargs={'edition_path': festival_entry.edition.path, 'pk': festival_entry.pk},
        )

        self.edition.is_archived = True
        self.edition.save()

        # Test that the owner cannot update an entry if the Edition is archived.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.save()

        # Test that the owner can update an entry.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(detail_url, response.url)

        festival_entry.refresh_from_db()
        self.assertEqual(title, festival_entry.title)
        self.assertEqual(description, festival_entry.description)
        self.assertEqual(credits, festival_entry.credits)
        self.assertEqual(website, festival_entry.website)
        self.assertEqual(video_link, festival_entry.video_link)

        # Test that the staff can update an entry.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data={**data, 'status': 'submitted', 'category': ''})
        self.assertEqual(302, response.status_code)
        self.assertEqual(detail_url, response.url)

        festival_entry.refresh_from_db()
        self.assertEqual(title, festival_entry.title)
        self.assertEqual(description, festival_entry.description)
        self.assertEqual(credits, festival_entry.credits)
        self.assertEqual(website, festival_entry.website)
        self.assertEqual(video_link, festival_entry.video_link)

        # Test that unrelated users cannot update an entry.
        self.client.force_login(self.unrelated_user)
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        # Test that anonymous users cannot update an entry.
        self.client.logout()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

        # Test that owner cannot update the status or category of an entry.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data={**data, 'status': 'winner', 'category': 'short'})
        self.assertEqual(302, response.status_code)
        self.assertEqual(detail_url, response.url)

        festival_entry.refresh_from_db()
        self.assertEqual('submitted', festival_entry.status)
        self.assertEqual('', festival_entry.category)

        # Test that staff users cannot leave the category empty when accepting an entry.
        for status in ('nominated', 'winner'):
            self.client.force_login(self.staff_user)
            response = self.client.post(url, data={**data, 'status': status, 'category': ''})
            self.assertEqual(200, response.status_code)
            self.assertIn(
                b'If the entry is nominated or has won a category should be set as well.',
                response.content,
            )

        # Test that staff users can reject an entry without specifying a category.
        self.client.force_login(self.staff_user)
        response = self.client.post(url, data={**data, 'status': 'rejected', 'category': ''})
        self.assertEqual(302, response.status_code)
        self.assertEqual(detail_url, response.url)

        festival_entry.refresh_from_db()
        self.assertEqual('rejected', festival_entry.status)
        self.assertEqual('', festival_entry.category)

        # Test that staff users can accept an entry and mark it as winner.
        for status in ('accepted', 'winner'):
            self.client.force_login(self.staff_user)
            response = self.client.post(url, data={**data, 'status': status, 'category': 'short'})
            self.assertEqual(302, response.status_code)
            self.assertEqual(detail_url, response.url)

            festival_entry.refresh_from_db()
            self.assertEqual(status, festival_entry.status)
            self.assertEqual('short', festival_entry.category)

            # Test that the owner cannot update the video link after an entry has been accepted.
            self.client.force_login(self.owner_user)
            response = self.client.post(
                url, data={**data, 'video_link': 'http://ww.google.nl/changed_after_acceptance'}
            )
            self.assertEqual(200, response.status_code)
            self.assertIn(
                b'You cannot change the video link after an entry has been accepted.',
                response.content,
            )

    def test_account_festival_entries_list(self):
        url = reverse('account_festival_entries_list')

        # Test that a user gets a message when he has no entries.
        self.client.force_login(self.owner_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertIn(b'Your Suzanne Awards Festival entries will show up here.', response.content)

        # Test that a user can see his entries.
        self.client.force_login(self.owner_user)
        self._create_festival_entry()
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertNotIn(
            b'Your Suzanne Awards Festival entries will show up here.', response.content
        )

        # Test an anonymous user cannot see any entries.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(302, response.status_code)

    def test_festival_entries_vote(self):
        url = reverse('festival_entries_vote', kwargs={'edition_path': self.edition.path})

        # Test that users do not need to login before viewing entries.
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        # Test that a user gets a message when there are no entries.
        self.client.force_login(self.unrelated_user)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertIn(b'There are no festival entries for this edition yet.', response.content)

        festival_entry = self._create_festival_entry()

        # Test that a user cannot see unaccepted entries.
        for status in ('submitted', 'rejected'):
            festival_entry.status = status
            festival_entry.save()

            self.client.force_login(self.unrelated_user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)
            self.assertIn(b'There are no festival entries for this edition yet.', response.content)

        # Test that a user can see accepted and winning entries.
        for status in ('accepted', 'winner'):
            festival_entry.status = status
            festival_entry.save()

            self.client.force_login(self.unrelated_user)
            response = self.client.get(url)
            self.assertEqual(200, response.status_code)
            self.assertNotIn(
                b'There are no festival entries for this edition yet.', response.content
            )

    def test_festival_entry_vote_post(self):
        festival_entry = self._create_festival_entry()
        url = reverse(
            'festival_entry_vote',
            kwargs={'edition_path': self.edition.path, 'pk': festival_entry.pk},
        )

        data = {'rating': 5}

        # Test voting needs to be open in order to vote.
        self.edition.is_archived = False
        self.edition.festival_voting_open = False
        self.edition.save()
        festival_entry.status = 'accepted'
        festival_entry.save()

        self.client.force_login(self.unrelated_user)
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        self.edition.festival_voting_open = True
        self.edition.save()

        # Test that users need to login before voting.
        self.client.logout()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)

        # Test that a user cannot vote on his own entry.
        self.client.force_login(self.owner_user)
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        # Test that an unrelated user cannot vote on non-accepted entries.
        for status in ('submitted', 'rejected'):
            festival_entry.status = status
            festival_entry.save()

            self.client.force_login(self.unrelated_user)
            response = self.client.post(url, data=data)
            self.assertEqual(403, response.status_code)

        # Test that an unrelated user can vote on accepted entries.
        for status in ('accepted', 'winner'):
            festival_entry.status = status
            festival_entry.save()

            self.client.force_login(self.unrelated_user)
            response = self.client.post(url, data=data)
            self.assertEqual(200, response.status_code)

    def test_message_festival_entry_form(self):
        festival_entry = self._create_festival_entry()

        message = 'זה כיף סתם לשמוע איך תנצח קרפד עץ טוב בגן.'

        url = reverse(
            'message_festival_entry_form',
            kwargs={'edition_path': festival_entry.edition.path, 'pk': festival_entry.pk},
        )
        data = {'message': message}

        festival_entry_url = reverse(
            'festival_entry_detail',
            kwargs={'edition_path': festival_entry.edition.path, 'pk': festival_entry.pk},
        )

        self.edition.is_archived = True
        self.edition.save()

        # Make sure the owner cannot post a message if `Edition.is_archived == True`.
        self.client.force_login(self.owner_user)
        mail.outbox.clear()
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        self.edition.is_archived = False
        self.edition.save()

        # Make sure the owner can post a message.
        self.client.force_login(self.owner_user)
        mail.outbox.clear()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(festival_entry_url, response.url)

        message_exchange: Optional[MessageExchange] = MessageExchange.objects.filter(
            message__user=self.owner_user
        ).first()
        self.assertIsNotNone(message_exchange)
        assert message_exchange is not None
        self.assertEqual(festival_entry, message_exchange.content_object)
        self.assertEqual(message, message_exchange.message.content)

        # Make sure an email notification was sent.
        self.assertEqual(1, len(mail.outbox))
        self.assertIn(festival_entry_url, mail.outbox[0].body)

        # Make sure a staff user can post a message.
        self.client.force_login(self.staff_user)
        mail.outbox.clear()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(festival_entry_url, response.url)

        message_exchange = MessageExchange.objects.filter(message__user=self.staff_user).first()
        self.assertIsNotNone(message_exchange)
        assert message_exchange is not None
        self.assertEqual(festival_entry, message_exchange.content_object)
        self.assertEqual(message, message_exchange.message.content)

        # Make sure an email notification was sent.
        self.assertEqual(1, len(mail.outbox))
        self.assertIn(festival_entry_url, mail.outbox[0].body)

        # Make sure an unrelated user cannot post a message.
        self.client.force_login(self.unrelated_user)
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)

        # Make sure an anonymous user cannot post a message.
        self.client.logout()
        response = self.client.post(url, data=data)
        self.assertEqual(302, response.status_code)
        self.assertEqual(login_url_with_redirect(url), response.url)

from typing import Optional

from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import redirect
from django.urls import include, path

import conference_main.admin
import conference_main.views.event
import conference_main.views.festival_entry
import conference_main.views.panel
import conference_main.views.photos
import conference_main.views.profile
from conference_main.models import SiteSettings


def redirect_to_current_edition(request):
    settings: Optional[SiteSettings] = getattr(get_current_site(request), 'settings', None)
    if settings is None or settings.current_edition is None:
        raise Exception(
            'Please create a SiteSettings object for the current site and set the current Edition.'
        )
    else:
        return redirect('homepage', edition_path=settings.current_edition.path, permanent=False)


urlpatterns = [
    path('', redirect_to_current_edition, name='homepage_redirect'),
    path('oauth/', include('blender_id_oauth_client.urls')),
    path(
        'account/profile/',
        conference_main.views.profile.ProfileUpdateView.as_view(),
        name='profile_update',
    ),
    path(
        'account/favorites/',
        conference_main.views.event.AccountFavoritesListView.as_view(),
        name='account_favorites_list',
    ),
    path(
        'account/presentations/',
        conference_main.views.event.AccountPresentationsListView.as_view(),
        name='account_presentations_list',
    ),
    path(
        '<str:edition_path>/festival/finals/',
        conference_main.views.festival_entry.FestivalEntriesFinalsListView.as_view(),
        name='festival_entries_finals',
    ),
    path(
        'account/festival/entries/',
        conference_main.views.festival_entry.AccountFestivalEntriesListView.as_view(),
        name='account_festival_entries_list',
    ),
    path(
        'profile/<int:pk>/',
        conference_main.views.profile.ProfileDetailView.as_view(),
        name='profile_detail',
    ),
    path(
        '<str:edition_path>/presentations/submit/',
        conference_main.views.event.PresentationSubmitView.as_view(),
        name='presentation_submit',
    ),
    path(
        '<str:edition_path>/presentations/json/',
        conference_main.views.event.PresentationListJSONView.as_view(),
        name='presentation_list_json',
    ),
    path(
        '<str:edition_path>/presentations/<int:pk>/',
        conference_main.views.event.PresentationDetailView.as_view(),
        name='presentation_detail',
    ),
    path(
        '<str:edition_path>/presentations/<int:pk>/review/',
        conference_main.views.event.PresentationReviewView.as_view(),
        name='presentation_review',
    ),
    path(
        '<str:edition_path>/presentations/<int:pk>/update/',
        conference_main.views.event.PresentationUpdateView.as_view(),
        name='presentation_update',
    ),
    path(
        '<str:edition_path>/presentations/<int:pk>/message/',
        conference_main.views.event.MessagePresentationFormView.as_view(),
        name='message_presentation_form',
    ),
    path(
        '<str:edition_path>/presentations/<int:pk>/favorite/',
        conference_main.views.event.PresentationFavoriteAjaxView.as_view(),
        name='presentation_favorite',
    ),
    path(
        '<str:edition_path>/presentations/<int:pk>/going/',
        conference_main.views.event.PresentationGoingAjaxView.as_view(),
        name='presentation_going',
    ),
    path(
        '<str:edition_path>/schedule/',
        conference_main.views.event.ScheduleView.as_view(),
        name='schedule',
    ),
    path(
        '<str:edition_path>/speakers/',
        conference_main.views.event.SpeakersView.as_view(),
        name='speakers',
    ),
    path(
        '<str:edition_path>/attendees/',
        conference_main.views.event.AttendeesView.as_view(),
        name='attendees',
    ),
    path(
        '<str:edition_path>/festival/entries/submit/',
        conference_main.views.festival_entry.FestivalEntrySubmitView.as_view(),
        name='festival_entry_submit',
    ),
    path(
        '<str:edition_path>/festival/entries/vote/',
        conference_main.views.festival_entry.FestivalEntriesVoteView.as_view(),
        name='festival_entries_vote',
    ),
    path(
        '<str:edition_path>/festival/entries/<int:pk>/',
        conference_main.views.festival_entry.FestivalEntryDetailView.as_view(),
        name='festival_entry_detail',
    ),
    path(
        '<str:edition_path>/festival/entries/<int:pk>/message/',
        conference_main.views.festival_entry.MessageFestivalEntryFormView.as_view(),
        name='message_festival_entry_form',
    ),
    path(
        '<str:edition_path>/festival/entries/<int:pk>/update/',
        conference_main.views.festival_entry.FestivalEntryUpdateView.as_view(),
        name='festival_entry_update',
    ),
    path(
        '<str:edition_path>/festival/entries/<int:pk>/vote/',
        conference_main.views.festival_entry.FestivalEntryVoteAjaxView.as_view(),
        name='festival_entry_vote',
    ),
    path(
        '<str:edition_path>/photos/<slug:album>/slideshow/',
        conference_main.views.photos.AlbumSlideshowView.as_view(),
        name='album_slideshow',
    ),
    path(
        '<str:edition_path>/photos/<slug:album>/upload/',
        conference_main.views.photos.AlbumUploadView.as_view(),
        name='album_upload',
    ),
    path(
        '<str:edition_path>/photos/<slug:album>/',
        conference_main.views.photos.AlbumView.as_view(),
        name='album',
    ),
    path(
        '<str:edition_path>/photos/',
        conference_main.views.photos.AlbumsView.as_view(),
        name='albums',
    ),
    path(
        '<str:edition_path>/panel/',
        conference_main.views.panel.PanelView.as_view(),
        name='panel',
    ),
    path(
        '<str:edition_path>/panel/location/<str:location_slug>/',
        conference_main.views.panel.PanelLocationView.as_view(),
        name='panel_location',
    ),
    path(
        '<str:edition_path>/panel/events/',
        conference_main.views.panel.EventsView.as_view(),
        name='panel_events',
    ),
]


# TinyMCE
urlpatterns += [
    path(
        'tinymce/conference_main/links/',
        conference_main.admin.TinyMCEFlatPagesLinkListView.as_view(),
        name='tinymce_flatpages_link_list',
    ),
    path(
        'tinymce/conference_main/images/',
        conference_main.admin.TinyMCEFlatFileImageListView.as_view(),
        name='tinymce_flatfile_image_list',
    ),
    path(
        'tinymce/conference_main/images/upload/',
        conference_main.admin.TinyMCEFlatFileImageUploadView.as_view(),
        name='tinymce_flatfile_image_upload',
    ),
]

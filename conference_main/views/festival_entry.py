from random import Random
from typing import Dict, Optional, Iterable, List

from django import urls
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.sites.models import Site
from django.db import models
from django.db.models import F, OuterRef
from django.db.models.aggregates import Sum
from django.db.models.expressions import Value, Subquery
from django.db.models.functions import Coalesce
from django.forms import BaseForm
from django.http import HttpResponseRedirect
from django.http.request import HttpRequest
from django.http.response import JsonResponse, HttpResponse
from django.views.generic import FormView, ListView, TemplateView
from django.views.generic.base import View
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.views.generic.edit import ModelFormMixin, ProcessFormView

from conference_main import permissions
from conference_main.forms import FestivalEntryForm, MessageForm, FestivalEntryStaffForm
from conference_main.models import (
    Edition,
    FestivalEntry,
    FestivalEntryFinalVotes,
    FestivalEntryVotes,
    Message,
    MessageExchange,
)
from conference_main.views import SingleObjectStoreMixin


class FestivalEntrySubmitView(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    """Form view for the submission of a entry for the festival."""

    # TODO(sem): Automatically prepend `http://` to URLs?
    template_name = 'conference_main/festival/entries/submit.pug'
    form_class = FestivalEntryForm

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: Edition = Edition.objects.get(path=self.kwargs['edition_path'])

    def has_permission(self):
        return permissions.can_add_festival_entry(self.edition, self.request.user)

    def get_success_url(self):
        return urls.reverse('account_festival_entries_list')

    def form_valid(self, form: BaseForm) -> HttpResponse:
        FestivalEntry.objects.create(
            user=self.request.user,
            edition=self.edition,
            category='',
            title=form.cleaned_data['title'],
            description=form.cleaned_data['description'],
            credits=form.cleaned_data['credits'],
            website=form.cleaned_data['website'],
            video_link=form.cleaned_data['video_link'],
        )

        return super().form_valid(form)


class AccountFestivalEntriesListView(LoginRequiredMixin, ListView):
    template_name = 'conference_main/festival/entries/account_list.pug'

    def get_queryset(self):
        return FestivalEntry.objects.filter(user=self.request.user).order_by(
            '-edition__year', '-created_at'
        )


class FestivalEntryDetailView(
    SingleObjectStoreMixin[FestivalEntry], PermissionRequiredMixin, TemplateView
):
    model = FestivalEntry

    def has_permission(self) -> bool:
        return permissions.can_view_festival_entry(self.object, self.request.user)

    def get_template_names(self):
        if self.request.headers.get('x-requested-with') == 'XMLHttpRequest':
            return ['conference_main/festival/entries/detail_ajax.pug']
        else:
            return 'conference_main/festival/entries/detail.pug'

    def get_context_data(self, **kwargs):
        vote: Optional[FestivalEntryVotes] = (
            FestivalEntryVotes.objects.filter(
                user=self.request.user, festival_entry=self.object
            ).first()
            if not self.request.user.is_anonymous
            else None
        )
        rating: Optional[int] = None if vote is None else vote.rating

        extra_context: Dict[str, object] = {
            'permissions': {
                'can_change': permissions.can_change_festival_entry(self.object, self.request.user),
                'can_view_messages': permissions.can_view_messages_on_festival_entry(
                    self.object, self.request.user
                ),
                'can_add_message': permissions.can_add_message_to_festival_entry(
                    self.object, self.request.user
                ),
                'can_vote': permissions.can_vote_on_festival_entry(self.object, self.request.user),
            },
            'message_form': MessageForm(),
            'rating': rating,
        }
        return super().get_context_data(**{**kwargs, **extra_context})


class FestivalEntryUpdateView(
    LoginRequiredMixin,
    SingleObjectStoreMixin[FestivalEntry],
    PermissionRequiredMixin,
    SingleObjectTemplateResponseMixin,
    ModelFormMixin,
    ProcessFormView,
):
    model = FestivalEntry
    template_name = 'conference_main/festival/entries/update.pug'

    def has_permission(self) -> bool:
        return permissions.can_change_festival_entry(self.object, self.request.user)

    def get_form_class(self):
        if self.request.user.has_perm('can_change_festival_entry'):
            return FestivalEntryStaffForm
        else:
            return FestivalEntryForm

    def get_success_url(self):
        return urls.reverse(
            'festival_entry_detail',
            kwargs={'edition_path': self.object.edition.path, 'pk': self.object.pk},
        )


class MessageFestivalEntryFormView(
    LoginRequiredMixin, SingleObjectStoreMixin[FestivalEntry], PermissionRequiredMixin, FormView
):
    """Form view for the submission of a Message on a FestivalEntry."""

    model = FestivalEntry
    form_class = MessageForm

    def has_permission(self) -> bool:
        return permissions.can_add_message_to_festival_entry(self.object, self.request.user)

    def form_valid(self, form):
        new_message = Message(content=form.cleaned_data['message'], user=self.request.user)
        new_message.save()
        new_message_exchange = MessageExchange(message=new_message, content_object=self.object)
        new_message_exchange.save()

        return HttpResponseRedirect(
            urls.reverse(
                'festival_entry_detail',
                kwargs={'edition_path': self.object.edition.path, 'pk': self.object.pk},
            )
        )


class FestivalEntriesVoteView(ListView):
    template_name = 'conference_main/festival/entries/vote.pug'
    context_object_name = 'festival_entries'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition = Edition.objects.get(path=kwargs['edition_path'])

    def get_queryset(self) -> Iterable[FestivalEntry]:  # type: ignore
        all_entries = FestivalEntry.objects.annotate(
            current_user_rating=Subquery(
                FestivalEntryVotes.objects.filter(
                    festival_entry_id=OuterRef('pk'), user=self.request.user
                ).values('rating')[:1]
            )
            if not self.request.user.is_anonymous
            else Value(None, models.BooleanField()),
            popularity=Coalesce(Sum(F('votes__rating')), Value(0)),
        ).filter(edition=self.edition, status__in=('accepted', 'winner'))

        number_of_entries = len(all_entries)

        if number_of_entries <= 1:
            return all_entries
        else:
            recency_score: Dict[FestivalEntry, float] = {
                entry: i_popularity / (number_of_entries - 1)
                for i_popularity, entry in enumerate(
                    sorted(all_entries, key=lambda e: e.created_at)
                )
            }

            popularity_score: Dict[FestivalEntry, float] = {
                entry: i_popularity / (number_of_entries - 1)
                for i_popularity, entry in enumerate(
                    sorted(all_entries, key=lambda e: e.popularity)
                )
            }

            site_settings = Site.objects.get_current(self.request).settings

            return sorted(
                all_entries,
                key=lambda e: (
                    10 if e.current_user_rating is None else e.current_user_rating,  # type: ignore
                    site_settings.recency_weight * recency_score[e]
                    + site_settings.popularity_weight * popularity_score[e]
                    + site_settings.per_user_random_weight
                    * Random(
                        e.id + (self.request.user.id if not self.request.user.is_anonymous else 0)
                    ).random(),
                ),
                reverse=True,
            )


class FestivalEntryVoteAjaxView(
    LoginRequiredMixin, SingleObjectStoreMixin[FestivalEntry], PermissionRequiredMixin, View
):
    model = FestivalEntry

    def has_permission(self) -> bool:
        return permissions.can_vote_on_festival_entry(self.object, self.request.user)

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> JsonResponse:
        rating: Optional[str] = self.request.POST.get('rating')

        if rating is None:
            return JsonResponse({'error': 'Missing required parameter "rating".'}, status=400)
        elif rating.isdigit() and 1 <= int(rating) <= 5:
            parsed_rating: int = int(rating)

            vote: Optional[FestivalEntryVotes] = FestivalEntryVotes.objects.filter(
                user=request.user, festival_entry=self.object
            ).first()

            if vote is None:
                FestivalEntryVotes.objects.create(
                    user=request.user, festival_entry=self.object, rating=parsed_rating
                )
            else:
                vote.rating = parsed_rating
                vote.save()
            return JsonResponse({'rating': parsed_rating})
        else:
            return JsonResponse(
                {'error': 'Field "rating" should be an integer from 1 to 5.'}, status=400
            )


class FestivalEntriesFinalsListView(LoginRequiredMixin, ListView):
    template_name = 'conference_main/festival/entries/finals_list.pug'

    def setup(self, request: HttpRequest, *args: str, **kwargs: str) -> None:
        super().setup(request, *args, **kwargs)
        self.edition: Edition = Edition.objects.get(path=self.kwargs['edition_path'])

    def get_queryset(self):
        return FestivalEntry.objects.filter(edition=self.edition, status='nominated')

    def has_permission(self) -> bool:
        entry_ids = map(int, self.request.POST.get('ratings').keys())
        return all(
            permissions.can_vote_on_festival_entry_final(
                FestivalEntry.objects.get(pk=entry_id), self.request.user
            )
            for entry_id in entry_ids
        )

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        object_list = context['object_list']
        per_category = {}
        for obj in object_list:
            title = obj.get_category_display()
            if title not in per_category:
                per_category[title] = []
            per_category[title].append(obj)
        context['per_category'] = per_category
        return context

    def post(self, request: HttpRequest, *args: str, **kwargs: str) -> JsonResponse:
        points_per_entry: Optional[List[int, int]] = {
            k: int(self.request.POST[k])
            for k in self.request.POST
            if k.startswith('points_for_entry_id_')
        }

        known_entry_ids = {entry.pk for entry in self.get_queryset()}
        # TODO: check sums of points per user and category
        for k, points in points_per_entry.items():
            entry_id = int(k.replace('points_for_entry_id_', ''))
            assert entry_id in known_entry_ids
            assert isinstance(points, int)
            if not (0 <= points <= 5):
                raise

            vote: Optional[FestivalEntryFinalVotes] = FestivalEntryFinalVotes.objects.filter(
                user=request.user, festival_entry_id=entry_id
            ).first()
            if points == 0 and vote:
                vote.delete()
            elif points > 0 and vote is None:
                FestivalEntryFinalVotes.objects.create(
                    user=request.user, festival_entry_id=entry_id, points=points
                )
            elif points > 0 and vote:
                vote.points = points
                vote.save()
        response = self.get(request, *args, **kwargs)
        response.context_data['has_voted'] = True
        return response

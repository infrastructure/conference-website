function tinyMCEImageUploadHandler(blobInfo, success, failure) {
    let xhr, formData;

    xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.open('POST', '/tinymce/conference_main/images/upload/');
    xhr.setRequestHeader("X-CSRFToken", Cookies.get('csrftoken'));

    xhr.onload = function () {
        let json;

        if (xhr.status != 200) {
            failure('HTTP Error: ' + xhr.status);
            return;
        }

        json = JSON.parse(xhr.responseText);

        if (!json || typeof json.location != 'string') {
            failure('Invalid JSON: ' + xhr.responseText);
            return;
        }

        success(json.location);
    };

    formData = new FormData();
    formData.append('file', blobInfo.blob(), blobInfo.filename());

    xhr.send(formData);
}

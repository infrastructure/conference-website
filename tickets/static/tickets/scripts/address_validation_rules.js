document.addEventListener('DOMContentLoaded', () => {
  const addressRulesPerCountryEl = document.getElementById('address-rules');
  if (!addressRulesPerCountryEl) {
    // Nothing to do: no country selector on the page (e.g. not logged in yet)
    return;
  }
  const addressRulesPerCountry = JSON.parse(addressRulesPerCountryEl.textContent);
  const countrySelectEl = document.getElementById('country');
  const countryAreaEl = document.getElementById('countryArea');
  const countryAreaElContainer = countryAreaEl.parentElement;
  const countryAreaLabel = document.querySelector('label[for="countryArea"]');

  function templateCountryAreaSelect(choices, label) {
    return `
      <option value="" selected="">Select ${label}</option>
      ${choices
        .map((area, index) => `<option value="${area[0]}">${area[1]}</option>`)
        .join('')}
  `;
  }

  function updateCountryArea(ev) {
    const selectedCountryCode = countrySelectEl.value;

    const rules = addressRulesPerCountry[selectedCountryCode];
    // console.log(selectedCountryCode, rules);
    if (rules && rules.require.country_area && rules.country_area_choices && rules.country_area_choices.length !== 0) {
      const label = rules.country_area_type;
      const choices = rules.country_area_choices;
      // Update country area choices accordingly
      countryAreaEl.innerHTML = templateCountryAreaSelect(choices, label);
      // We use a patched Saleor 3.5 in combination with
      // checkRequiredFields: false to make most of the address fields optional.
      // countryAreaEl.required = true;
      countryAreaLabel.innerHTML = `${label}`;
      // Show the updated select
      countryAreaElContainer.classList.remove('d-none');

      // Set the current value if supplied via a forceUpdate event
      if (ev && ev.detail && ev.detail.value && ev.detail.value !== countryAreaEl.value) {
        countryAreaEl.value = ev.detail.value;
      }
      return;
    }
    // Reset and hide the country area field
    countryAreaEl.innerHTML = '';
    // countryAreaEl.required = false;
    countryAreaLabel.innerText = 'Country Area';
    countryAreaElContainer.classList.add('d-none');
  }

  function updateAddressFields(ev) {
    updateCountryArea(ev);
  }

  if (!!countrySelectEl && !!countryAreaEl && !!countryAreaLabel) {
    // Update address fields according to validation rules for the selected country
    countrySelectEl.addEventListener('change', updateAddressFields);
    // Update address fields on a custom event, e.g. when country value after DOMContentLoaded
    countryAreaEl.addEventListener('forceUpdate', updateAddressFields);
  }
});

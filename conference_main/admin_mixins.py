import csv
from django.http import HttpResponse


class NoAddDeleteMixin:
    """Disallow adding and deleting objects via the admin."""

    def has_add_permission(self, *args, **kwargs):
        """Disallow adding new objects via the admin."""
        return False

    def has_delete_permission(self, *args, **kwargs):
        """Disallow removing objects via the admin."""
        return False


class ExportCsvMixin:
    def export_as_csv(self, request, queryset):

        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            writer.writerow([getattr(obj, field) for field in field_names])

        return response

    export_as_csv.short_description = 'Export selected as CSV'

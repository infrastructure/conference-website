Ticket{{ ticket.quantity|pluralize }} information:
-------------------------
{% spaceless %}{% include "tickets/components/product_title" %}{% endspaceless %}
Order number:       {% firstof order.number ticket.order_number %}{% if ticket.quantity > 1 %}
Number of tickets:  {{ ticket.quantity }}{% endif %}
{% with unclaimed=ticket.unclaimed %}{% if unclaimed %}Unclaimed tickets:  {{ unclaimed }}{% endif %}{% endwith %}
Total:              {% spaceless %}{% include "tickets/components/ticket_order_total.txt" %}{% endspaceless %}

{% extends "emails/base.txt" %}{% block content %}
Your {{ edition }} ticket confirmed!
{% include "emails/components/ticket_info.txt" %}
{% with ticket_url=ticket.get_absolute_url %}
Review the ticket here:
{{ site_url }}{{ ticket_url }}
{% endwith %}
See you at the Conference!{% endblock %}

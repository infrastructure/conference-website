# Generated by Django 4.2.13 on 2024-05-27 13:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0006_product_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='taxes',
            field=models.JSONField(
                blank=True, 
                help_text="Breakdown of taxes: only used for display, not for calculating paid totals. Total price is set on the payment backend's product, and assumed to always include all taxes.",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name='ticket',
            name='product',
            field=models.ForeignKey(
                blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='tickets.product'
            ),
        ),
    ]

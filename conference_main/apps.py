from django.apps import AppConfig


class ConferenceMainConfig(AppConfig):
    name = 'conference_main'
    verbose_name = 'Blender Conference'

    def ready(self):
        import conference_main.signals  # noqa

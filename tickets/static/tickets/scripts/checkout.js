// Our BrainTree interface stuff for payment.
// Using it requires the following Braintree scripts:
//   https://js.braintreegateway.com/web/3.68.0/js/data-collector.min.js
//   https://js.braintreegateway.com/web/dropin/1.25.0/js/dropin.min.js
//
// Optionally allows reCAPTCHA to be requested before a payment method
// can be sent to the payment gateway and our back-end. In this case,
// a verification token is included into the form payload,
// if reCAPTCHA challenge was completed.
//

/* Fires a custom event indicating that Google reCAPTCHA has loaded.
 * Must exist in the global scope to be accessible to reCAPTCHA's API.
 * Allows reCAPTCHA to be later configured with a callback
 * defined outside the global scope.
 */
function recaptchaOnLoad() {
  const event = new Event('recaptchaLoaded');
  document.dispatchEvent(event);
}

document.addEventListener('DOMContentLoaded', function() {
  const typedQueries = new TypedQueries();
  const utils = new CheckoutUtils();
  const client = utils.getClient();

  const countryCode = utils.getCountryCode() || 'NL';
  const META_VATIN_KEY = 'vatrc.vatin';

  const priceEl = document.getElementById('id_amount');
  const bankTransferRadioEl = document.getElementById('id_gateway_1');
  const bankTransferOptionEl = bankTransferRadioEl.parentElement;
  const bankTransferOptionWarnEl = document.getElementById('bank-transfer-warning');
  const countrySelectEl = document.getElementById('country');
  const regionSelectEl = document.getElementById('countryArea');
  const checkoutLinesEl = document.getElementById('checkout-lines');
  const checkoutTotalsEl = document.getElementById('checkout-totals');

  /* Payment gateway setup */
  // See below declarations for details about what HTML elements
  // are required for this script to work.
  const billingAddressFieldset = document.getElementById('billing-address');
  const totalsFieldset = document.getElementById('totals');
  const recaptcha = document.getElementById('recaptcha');
  const checkoutForm = document.getElementById('checkout-form');
  const promoCodeEl = document.getElementById('promoCode');
  const gatewayErrors = document.getElementById('gateway-errors');

  const submitButton = document.getElementById('submit-button');
  const submitButtonHTML = (submitButton) ? submitButton.innerHTML : '';

  // Form fields are based on Looper's model forms,
  // and are expected to be templated with default Django form magic.
  const paymentMethodNonce = document.getElementById('id_payment_method_nonce');
  const deviceData = document.getElementById('id_device_data');

  const gatewayRadio = document.querySelectorAll('[name=gateway]');
  const gatewayRadioBank = document.querySelector('[name=gateway][value=bank]');
  const gatewayRadioBraintree = document.querySelector('[name=gateway][value=braintree]');

  const checkoutToken = checkoutForm.dataset.token;

  const braintreeContainerId = 'bt-dropin';
  const btDropinStyleOverridesEl = document.getElementById('bt-dropin-styles');
  let btDropinStyleOverrides = {};
  if (btDropinStyleOverridesEl) {
    btDropinStyleOverrides = JSON.parse(btDropinStyleOverridesEl.textContent);
  }

  // braintreeClient will be initialized later.
  let braintreeClient = null;
  let braintreeCreateCalled = false;

  let checkout;
  let updatingCheckoutInProgress = false;
  let updateLinesInProgress = false;
  let updatedVATIN = null;

  function validateAndSubmit(form) {
    disableSubmitButton();

    // requestSubmit is not supported in Safari WebKit (and IE).
    // However, validation popovers are still displayed in case of invalid fields,
    // and we can use `checkValidity` which better support.
    if (checkoutForm.checkValidity()) {
      // Proceed to submit the form
      checkoutForm.submit();
    } else {
      enableSubmitButton();
    }
  }

  function enableSubmitButton() {
    submitButton.disabled = false;
    submitButton.setAttribute('aria-disabled', 'false');
    submitButton.classList.remove('disabled');
    submitButton.classList.add('btn-success');
    submitButton.innerHTML = submitButtonHTML;
  }

  function disableSubmitButton(text) {
    submitButton.disabled = true;
    submitButton.setAttribute('aria-disabled', 'true');
    submitButton.classList.add('disabled');
    submitButton.classList.remove('btn-success');
    if (text) {
      submitButton.innerText = text;
    }
    showBankTransferOption();
  }

  function gatherChallengeData() {
    // See https://developers.braintreepayments.com/guides/3d-secure/client-side/javascript/v3#drop-in-ui
    let fields = {};
    const fieldNames = [
      'firstName',
      'lastName',
      'streetAddress1',
      'city',
      'postalCode',
      'country',
      'email'
    ];
    for (fieldName of fieldNames) {
      fields[fieldName] = checkoutForm.elements[fieldName].value;
    }
    console.log('gatherChallengeData', fields);
    fields['price'] = priceEl.value;
    return {
      amount: parseFloat(fields.price),
      email: fields.email,
      billingAddress: {
        // FIXME(anna): make sure these are ASCII
        givenName: fields.firstName,
        surname: fields.lastName,
        streetAddress: fields.streetAddress1,
        // extendedAddress: fields['extended_address'].input.value,
        locality: fields.city,
        // region: fields['region'].input.value,
        postalCode: fields.postalCode,
        countryCodeAlpha2: fields.country
      }
    };
  }

  function getDropinEl() {
    return document.getElementById(braintreeContainerId);
  }

  function isGatewayBraintreeSelected() {
    return gatewayRadioBraintree && gatewayRadioBraintree.checked;
  }

  function isGatewayBankSelected() {
    return gatewayRadioBank && gatewayRadioBank.checked;
  }

  function getRecaptchaResponseEl() {
    return document.getElementById('g-recaptcha-response');
  }

  function getGatewayErrorDetail(err) {
    try {
      return err.details.originalError.details.originalError.error.message;
    } catch (err) {
      return null;
    }
  }

  function initSelectedPaymentGateway(braintreeOptions) {
    utils.clearErrors(checkoutForm);

    getDropinEl().classList.add('show');
    // Braintree is selected, initialising Braintree DropIn UI
    if (braintreeCreateCalled || braintreeClient) {
      // Already initialized
      return;
    }
    // Initialise Braintree UI
    disableSubmitButton('Loading...');
    braintree.dropin.create(braintreeOptions, function(createErr, instance) {
      if (createErr != null) {
        console.error(createErr);
        // const msg = gatherErrorMsg(createErr);

        disableSubmitButton('Error loading payment interface');
        return;
      }
      braintreeClient = instance;
      disableSubmitButton('Select a payment method');
      braintreeClient.on('paymentMethodRequestable', enableSubmitButton);
      braintreeClient.on('noPaymentMethodRequestable', disableSubmitButton);
      braintreeClient.on('paymentOptionSelected', hideBankTransferOption);
      // There doesn't seem to be an event notifying about clicks on "Choose another way to pay".
      // This allows to bring back the bank option when payment view changes back to options.
      const btToggleEls = document.getElementsByClassName('braintree-toggle');
      if (btToggleEls.length) {
        const btToggleEl = btToggleEls[0];
        btToggleEl.addEventListener('click', toggleBankTransferOption);
      }
    });
    braintreeCreateCalled = true;
  }

  function toggleBankTransferOption() {
    if (bankTransferOptionEl.classList.contains('d-none')) {
      showBankTransferOption();
    } else {
      hideBankTransferOption();
    }
  }

  function showBankTransferOption() {
    bankTransferOptionEl.classList.remove('d-none');
  }

  function hideBankTransferOption() {
    bankTransferOptionEl.classList.add('d-none');
    bankTransferOptionWarnEl.classList.add('d-none');
  }

  function displayValidationErrorForVATIN() {
    // Display a validation error in case VATIN was submitted but not saved as valid:
    const vatinInMetadata = utils.getMetadataValueByKey(checkout.metadata, META_VATIN_KEY) || '';
    if (!updatedVATIN) return;
    if (updatedVATIN === vatinInMetadata) return;

    const vatinErr = {
      // Keep displaying this error until the next update
      keep: true,
      field: 'vatin',
      message: `${updatedVATIN} is invalid or doesn't match the billing country.`
    }
    utils.displayFieldValidationError(checkoutForm, vatinErr);
  }

  function displayCheckoutData() {
    utils.clearErrors(checkoutForm);
    displayBillingAddress();
    displayCheckoutLines();
    displayValidationErrorForVATIN();
    promoCodeEl.value = checkout.voucherCode;
    utils.enableFieldset(billingAddressFieldset);
    utils.enableFieldset(totalsFieldset);
  }

  function formatCheckoutLine(line) {
    const variant = line.variant;
    const product = variant.product;
    const price = line.totalPrice;
    const imgURL = product.thumbnail.url;
    return `<div class="card shadow-none my-2"
        data-line-id="${line.id}" data-variant-id="${variant.id}">
      <div class="row">
        <div class="col-md-2"
             style="
               margin-left: 1em;
               margin-right: -1em;
               background-image: url(${imgURL});
               background-size: cover;
               border-radius: var(--border-radius);
             ">
        </div>
        <div class="col-md-10 col-sm">
          <div class="row">
            <div class="col-md-6 col-sm-8">
              <div class="ml-3 mt-3 mb-3">
                <span class="d-block">${product.name}</span>
                <small>${variant.name}</small>
              </div>
            </div>
            <div class="col-md-3 col-sm-4">
              <div class="ml-3 mt-3 mr-xs-1">
                <input type="number" class="form-control quantity-control" value="${line.quantity}" data-variant-id="${line.variant.id}" min="1">
              </div>
            </div>
            <div class="col-md-3 text-right">
              <div class="mr-3 mt-3">
                <h5 class="no-breaks">${utils.formatMoney(price.gross)}</h5>
              </div>
            </div>
          </div>
          <!--div class="row">
            <div class="col-8"></div>
            <div class="col-4 text-right">
              <div class="mb-3 mr-3 no-breaks">
                ` + ((price.tax.amount > 0) ? `<small>incl&nbsp;VAT&nbsp;${utils.formatMoney(price.tax)}</small>` : '') +
                `
              </div>
            </div>
          </div-->
        </div>
      </div>
    </div>`;
  }

  function formatCheckoutTotals(checkout) {
    const price = checkout.totalPrice;
    return `
    <div class="mb-3 mr-3">
      <h4>${utils.formatMoney(price.gross)}</h4>
    </div>
    <div class="mb-3 mr-3">
      ` + ((price.tax.amount > 0) ? `<small>incl&nbsp;VAT&nbsp;${utils.formatMoney(price.tax)}</small>` : '') +
      `
    </div>`;
  }

  function displayCheckoutLines() {
    // Display the checkout lines with their prices and quantity
    checkoutLinesEl.innerHTML = '';
    checkout.lines.forEach(function(line) {
      const el = document.createElement('div');
      el.innerHTML = formatCheckoutLine(line);
      checkoutLinesEl.appendChild(el);
    });

    // Set price input value for Braintree DropIn
    priceEl.value = checkout.totalPrice.gross.amount;

    // Display the totals
    checkoutTotalsEl.innerHTML = '';
    const el = document.createElement('div');
    el.classList.add('no-breaks');
    el.innerHTML = formatCheckoutTotals(checkout);
    checkoutTotalsEl.appendChild(el);
  }

  function updateBillingAddressIfFilled(callback) {
    // Only attempt further steps if all required fields are filled in.
    let canProceed = true;
    for(let i = 0; i < billingAddressFieldset.elements.length; i ++) {
      const el = billingAddressFieldset.elements[i];
      const hasFocus = el === document.activeElement;
      if (!el.value && el.required || !el.checkValidity() || hasFocus) {
        canProceed = false;
        break;
      }
    };
    if (!canProceed) return false;

    utils.clearErrors(checkoutForm);
    if (!checkoutForm.checkValidity()) {
      checkoutForm.reportValidity();
      return false;
    }
    updateBillingAddress(callback);
  }

  function onUpdateBillingAddressDone() {
    utils.enableFieldset(billingAddressFieldset);
    utils.enableFieldset(totalsFieldset);
    updatingCheckoutInProgress = false;
  }

  function updateCheckoutMetadata(formData) {
    // Update checkout metadata with VATIN, if provided.
    updatedVATIN = (formData) ? formData.vatin.replace(/\s+/g, '') : '';
    const metadataMutation = typedQueries.getUpdateMetadata(
      checkoutToken, [{key: META_VATIN_KEY, value: updatedVATIN}]
    );
    return client.mutate({
      mutation: metadataMutation,
      // avoid checking cache, otherwise Apollo will return stale metadata
      fetchPolicy: 'network-only'
    });
  }

  function updateBillingAddress(callback) {
    // Save updated billing address
    if (updatingCheckoutInProgress) {
      return false;
    }
    updatingCheckoutInProgress = true;

    utils.disableFieldset(billingAddressFieldset);
    utils.disableFieldset(totalsFieldset);
    const formData = utils.getFormFieldsValues(billingAddressFieldset);
    const mutation = typedQueries.getCheckoutBillingAddressUpdateMutation(
        checkoutToken, formData
    );

    // Metadata must be updated first, otherwise its change will not be
    // available when plugins get to calculate taxes.
    updateCheckoutMetadata(formData).then(function(metadataResponse) {
      client.mutate({
        mutation,
        // avoid checking cache, otherwise Apollo will return stale metadata
        fetchPolicy: 'network-only'
      }).then(function(response) {
        const data = response.data.checkoutBillingAddressUpdate;
        if (!data.checkout) {
          // Handle errors, if any
          const errors = data.errors;
          let message = '';
          if (errors && errors.length) {
            errors.forEach(function(err) {
              console.error(err);
              if (err.field) {
                utils.displayFieldValidationError(checkoutForm, err);
              } else {
                message += ((err.field) ? `${err.field}: ` : '') + err.message + '<br>';
              }
            });
            if (message) {
              utils.displayError(message);
            }
          }
          onUpdateBillingAddressDone();
        } else {
          fetchCheckout();
          if (callback) callback();
        }
      });
    });
  }

  function onBankTransferOptionClick(e) {
    bankTransferOptionWarnEl.classList.remove('d-none');
    enableSubmitButton();
  }

  function onLineQuantityChange(e) {
    if (e.target && e.target.classList.contains('quantity-control')) {
      e.preventDefault();
      // Stop from other form handlers from firing and causing race conditions
      e.stopImmediatePropagation();
      e.target.disabled = true;
      updateLines(e);
      e.target.disabled = false;
      return false;
    }
  }

  function onPromoCodeChange(e) {
    // Add or remove a voucher code

    // Stop from other form handlers from firing and causing race conditions
    e.stopImmediatePropagation();
    if (updatingCheckoutInProgress) {
      return false;
    }
    updatingCheckoutInProgress = true;

    const el = e.target;
    const newCode = el.value;

    const mutationName = (!newCode && checkout.voucherCode) ?
      'checkoutRemovePromoCode' : 'checkoutAddPromoCode';
    const code = (mutationName === 'checkoutRemovePromoCode') ? checkout.voucherCode : newCode;
    const mutation = typedQueries.getCheckoutPromoCodeMutation(checkoutToken, code, mutationName);
    client.mutate({
      mutation: mutation,
      // avoid checking cache, otherwise Apollo will return stale metadata
      fetchPolicy: 'network-only'
    }).then(function(results) {
      const data = results.data[mutationName];
      if (!data.checkout) {
        // Handle errors, if any
        const errors = data.errors;
        let message = '';
        if (errors && errors.length) {
          errors.forEach(function(err) {
            console.error(err);
            if (err.field) {
              utils.displayFieldValidationError(checkoutForm, err);
            } else {
              message += ((err.field) ? `${err.field}: ` : '') + err.message + '<br>';
            }
          });
          if (message) {
            utils.displayError(message);
          }
        }
        onUpdateBillingAddressDone();
      } else {
        checkout = data.checkout;
        displayCheckoutData();
        onUpdateBillingAddressDone();
      }
    });
  }

  function fetchCheckout(onError) {
    // Fetch checkout data
    client.query({
      query: typedQueries.getCheckoutQuery(checkoutToken),
      // avoid checking cache, otherwise Apollo will return stale metadata
      fetchPolicy: 'network-only'
    }).then(function(response) {
      checkout = response.data.checkout;
      displayCheckoutData();
      onUpdateBillingAddressDone();
    });
  }

  function updateLines(ev) {
    // Update checkout lines
    if (updateLinesInProgress) {
      return false;
    }
    updateLinesInProgress = true;

    const quantity = ev.target.value;
    const variantId = ev.target.dataset.variantId;
    const mutation = typedQueries.getCheckoutLinesUpdateMutation(
        checkoutToken, variantId, quantity
    );

    client.mutate({
      mutation,
      // avoid checking cache, otherwise Apollo will return stale metadata
      fetchPolicy: 'network-only'
    }).then(function(response) {
      const data = response.data.checkoutLinesUpdate;
      if (!data.checkout) {
        // Handle errors, if any
        const errors = data.errors;
        let message = '';
        if (errors && errors.length) {
          errors.forEach(function(err) {
            console.error(err);
            message += ((err.field) ? `${err.field}: ` : '') + err.message + '<br>';
          });
          if (message) {
            utils.displayError(message);
          }
        }
      } else {
        fetchCheckout();
      }
      updateLinesInProgress = false;
    });
  }

  function displayBillingAddress() {
    // VATIN is a special case, it's stored in checkout metadata:
    const vatin = utils.getMetadataValueByKey(checkout.metadata, META_VATIN_KEY) || '';
    const formFieldEl = billingAddressFieldset.elements['vatin'];
    if (formFieldEl) formFieldEl.value = vatin;

    const address = checkout.billingAddress;
    // Skip if billingAddress hadn't been filled yet
    if (!address) return;

    Object.keys(address).forEach(function(field) {
      const value = address[field];
      const formFieldEl = billingAddressFieldset.elements[field];
      // Get country code value if this is 'country'
      if (formFieldEl) formFieldEl.value = (value.code) ? value.code : value;
    });

    // Update region select now that country value is known
    regionSelectEl.dispatchEvent(
        new CustomEvent('forceUpdate', {detail: {value: address.countryArea}})
    );
  }

  function getClientToken(gateways, gatewayName) {
    let clientToken;
    gateways.forEach(function(gw) {
      if (gw.name === gatewayName) {
        gw.config.forEach(function(configValue) {
          if (configValue.field === 'client_token') {
            clientToken = configValue.value;
          }
        });
      }
    });
    return clientToken;
  }

  function displayPaymentMethods(response) {
    checkout = response.data.checkout;
    const gws = checkout.availablePaymentGateways;
    const braintreeClientToken = getClientToken(gws, 'Braintree');
    const braintreeOptions = {
      authorization: braintreeClientToken,
      // This is why the container must be a single element with an ID:
      container: '#' + braintreeContainerId,
      paypal: {
        flow: 'vault',
        overrides: {styles: btDropinStyleOverrides}
      },
      card: {
        overrides: {styles: btDropinStyleOverrides}
      },
      preselectVaultedPaymentMethod: false,
      dataCollector: {
        kount: false // true only if Kount fraud data collection is enabled
      },
      threeDSecure: true
    };
    initSelectedPaymentGateway(braintreeOptions);
    displayCheckoutData();
    checkoutForm.onchange = utils.throttleFunction(updateBillingAddressIfFilled, 1000);
  }

  /* Called by the reCAPTCHA v2 API after a successful verification. */
  function recaptchaSuccessful(token) {
    if (recaptcha) {
      const recaptchaEl = getRecaptchaResponseEl();
      recaptchaEl.textContent = token;
    }

    if (isGatewayBraintreeSelected()) {
      const challenge = {threeDSecure: gatherChallengeData()};
      //console.log('challenge: ', challenge);

      disableSubmitButton('Processing...');

      braintreeClient.requestPaymentMethod(challenge, function(err, payload) {
        if (err) {
          console.error(err);
          // After an error, the button should be re-enabled to allow a retry.
          enableSubmitButton();
          // TODO(fsiddi): Investigate the API to properly handle the case of a selected, but not clicked
          // PayPal checkout button.
          if (err.message === 'No payment method is available.') {
            err.message += ' If you selected PayPal, click on the Yellow PayPal Checkout button.';
          }
          const errorMsg = getGatewayErrorDetail(err) || err.message;
          utils.displayError(errorMsg);
          braintreeClient.clearSelectedPaymentMethod();
          return;
        }

        if (payload.liabilityShiftPossible && !payload.liabilityShifted) {
          // This payment method was eligible for 3D Secure
          // and the customer failed 3D Secure authentication
          utils.displayError('Security challenge failed');
          braintreeClient.clearSelectedPaymentMethod();
          // After an error, the button should be re-enabled to allow a retry.
          enableSubmitButton();
          return;
        }

        // Either the payment method is not eligible, or the challenge succeeded
        //console.log('verification success or not eligible:', payload);
        // Add the nonce to the form and submit (input field is created by the CheckoutForm)
        paymentMethodNonce.value = payload.nonce;
        deviceData.value = payload.deviceData;
        updateBillingAddressIfFilled(validateAndSubmit);
      });
    } else if (isGatewayBankSelected()) {
      updateBillingAddressIfFilled(validateAndSubmit);
    }
  }

  function submitFormWithRecaptcha() {
    if (recaptcha != null) {
      // Make the user pass a recaptcha.

      /* reCAPTCHA won't execute twice. However, the user can still click the button twice.
       * This happens for example once before checking the "Another Membership" checkbox,
       * at which time the browser may block the form submission because the checkbox is
       *  marked required, and then another click after checking the checkbox. */
      const recaptchaEl = getRecaptchaResponseEl();
      if (recaptchaEl.textContent) {
        recaptchaSuccessful(recaptchaEl.textContent);
        return;
      }

      grecaptcha.execute();
      enableSubmitOnRecaptchaClose();
    } else {
      recaptchaSuccessful();
    }
  }

  function enableSubmitOnRecaptchaClose() {
    // Try to find out if reCAPTCHA has been closed to re-enable the submit button
    try {
      const iframe = document.querySelector(
          'iframe[src^="https://www.google.com/recaptcha"][src*="bframe"]'
      );
      const container = iframe.parentNode.parentNode;
      const observer = new MutationObserver((mutations) => {
        if (container && container.style.visibility === 'hidden') {
          enableSubmitButton();
          observer.disconnect();
        }
      });
      observer.observe(container, {attributes: true, attributeFilter: ['style']});
    } catch (error) {
      // The try-catch was added here out of caution
      // (any uncaught error here would break the payment flow)
      // rather than expectations of any specific errors.
      console.error(error);
    }
  }

  // Select country fetched from current locale
  if (countryCode) countrySelectEl.value = countryCode;

  // Fetch current checkout data.
  if (checkoutToken && window.location.pathname.includes(checkoutToken)) {
    client.query({
      query: typedQueries.getCheckoutQuery(checkoutToken),
      // avoid checking cache, otherwise Apollo will return stale metadata
      fetchPolicy: 'network-only'
    }).then(function(response) {
      const checkout = response.data.checkout;
      // If checkout is not valid, redirect
      if (checkout === null) {
        // TODO(anna): checkout view must do the redirect, this check is non-issue.
        console.error('INVALID CHECKOUT TOKEN', checkoutToken);
      } else {
        displayPaymentMethods(response);
      }
    });
  }

  // Handle payment button
  checkoutForm.addEventListener('submit', (event) => {
    // Disable default form submission, since we submit the form
    // explicitly in the `recaptchaSuccessful` callback.
    event.preventDefault();
    event.stopPropagation();

    utils.clearErrors(checkoutForm);
    disableSubmitButton('Processing..');

    submitFormWithRecaptcha();
  });

  // Handle bank transfer option
  bankTransferOptionEl.addEventListener('click', onBankTransferOptionClick);

  // Handle quantity changes
  checkoutForm.addEventListener('change', onLineQuantityChange);

  // Handle promo code changes
  promoCodeEl.addEventListener('change', onPromoCodeChange);
});

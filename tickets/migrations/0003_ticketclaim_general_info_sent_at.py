# Generated by Django 3.2.13 on 2022-09-07 11:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0002_ticket_invoice'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticketclaim',
            name='general_info_sent_at',
            field=models.DateTimeField(blank=True, help_text='Date when general info email was sent to this attendee.', null=True),
        ),
    ]

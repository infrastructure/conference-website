from typing import Set

from django.contrib.auth import get_user_model
from django.db.models.signals import m2m_changed
from django.dispatch import receiver

import tickets.models
import tickets.tasks as tasks

User = get_user_model()


@receiver(m2m_changed, sender=tickets.models.Ticket.attendees.through)
def _on_ticket_claimed(
    sender: object, instance: tickets.models.Ticket, pk_set: Set[int], action: str, **kwargs: object
) -> None:
    if kwargs.get('raw', False):
        return

    if action != 'post_add':
        return

    # Send out additional emails to added attendees if ticket is paid or free
    if instance.is_paid or instance.is_free:
        for user_id in pk_set:
            tasks.send_mail_confirm_tickets(ticket_id=instance.pk, user_id=user_id)
            # TODO don't send general info yet, start when text is confirmed
            # Also send out general info
            # tasks.send_mail_general_info(ticket_id=instance.pk, user_id=user_id)

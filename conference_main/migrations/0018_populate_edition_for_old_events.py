from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [('conference_main', '0017_festival_entry_website_optional')]

    operations = [
        migrations.RunSQL(
            sql='''
                UPDATE
                    conference_main_event
                SET
                    edition_id = conference_main_day.edition_id
                FROM
                     conference_main_day
                WHERE
                      conference_main_day.id = conference_main_event.day_id;
            ''',
            reverse_sql=migrations.RunSQL.noop,
        )
    ]

from django import forms

import conference_main.models


class NewCheckoutForm(forms.Form):
    quantity = forms.IntegerField(min_value=1, max_value=50, initial=1, required=True, label='')
    variant_id = forms.CharField(
        min_length=24, max_length=24, required=True, widget=forms.HiddenInput()
    )


class GatewayRadioSelect(forms.RadioSelect):
    option_template_name = 'tickets/forms/widgets/gateway_radio_option.html'


class CheckoutCompleteForm(forms.Form):
    amount = forms.CharField(widget=forms.HiddenInput(), required=True)
    payment_method_nonce = forms.CharField(initial='pending', widget=forms.HiddenInput())
    gateway = forms.ChoiceField(
        choices=(
            ('braintree', 'Credit card or PayPal'),
            ('bank', 'Bank transfer'),
        ),
        initial='braintree',
        required=True,
        widget=GatewayRadioSelect(attrs={'autocomplete': 'off'}),
        label='',
    )
    device_data = forms.CharField(
        initial='set-in-javascript', widget=forms.HiddenInput(), required=False
    )


class BadgeForm(forms.ModelForm):
    class Meta:
        model = conference_main.models.Profile
        fields = ('full_name', 'title', 'company', 'country')

    def __init__(self, *args, **kwargs):
        """Make name required, so there was something to be displayed on the badge."""
        super().__init__(*args, **kwargs)
        self.fields['full_name'].required = True

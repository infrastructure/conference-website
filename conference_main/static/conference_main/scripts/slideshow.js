function getLatestPhoto() {
	let slideshow = document.getElementById("slideshow");
	let authorFullName = document.getElementById("js-author-full-name");
	let authorTitle = document.getElementById("js-author-title");
	let authorCompany = document.getElementById("js-author-company");
	$.get({
		url: slideshow.dataset.slideshowUrl,
		data: {'response_type': 'json'},
		success: function (data) {
			if (data.photo && slideshow.dataset.photo !== data.photo) {
				let photo = new Image();
				photo.onload = function () {
					slideshow.src = photo.src;
					slideshow.dataset.photo = data.photo;
				};
				photo.src = data.photo;
			}

			authorFullName.innerText = data.authorFullName ? data.authorFullName : '' ;
			authorTitle.innerText = data.authorTitle ? data.authorTitle : '' ;
			authorCompany.innerText = data.authorCompany ? data.authorCompany : '' ;
		},
		complete: function () {
			setTimeout(getLatestPhoto, 1000)
		}
	})
}

$(document).ready(function () {
	setTimeout(getLatestPhoto, 1000);
});

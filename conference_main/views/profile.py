import itertools
from typing import Dict, List

from django import urls
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.sites.shortcuts import get_current_site
from django.views.generic.base import TemplateView
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.views.generic.edit import ProcessFormView, ModelFormMixin

from conference_main import permissions
from conference_main.forms import ProfileForm
from conference_main.models import Profile, Event
from conference_main.views import SingleObjectStoreMixin


class ProfileUpdateView(
    LoginRequiredMixin,
    SingleObjectStoreMixin[Profile],
    SingleObjectTemplateResponseMixin,
    ModelFormMixin,
    ProcessFormView,
):
    model = Profile
    form_class = ProfileForm
    template_name = 'conference_main/profile/update.pug'

    def get_object(self, queryset=None):
        if self.request.user.is_authenticated:
            return Profile.objects.get(user=self.request.user)
        else:
            return None

    def form_valid(self, form):
        messages.success(self.request, "Profile details updated.")
        return super().form_valid(form)

    def get_success_url(self):
        return urls.reverse('profile_update')


class ProfileDetailView(SingleObjectStoreMixin[Profile], PermissionRequiredMixin, TemplateView):
    model = Profile
    template_name = 'conference_main/profile/detail.pug'
    context_object_name = 'profile'

    def has_permission(self) -> bool:
        return permissions.can_view_profile(self.object, self.request.user)

    def get_context_data(self, **kwargs: object) -> Dict[str, object]:
        ctx = super().get_context_data(**kwargs)
        site_settings = get_current_site(self.request).settings
        ctx['edition'] = site_settings.current_edition

        events_per_edition: Dict[str, List[Event]] = {
            title: list(events)
            for (_, title), events in itertools.groupby(
                self.object.events.filter(status='accepted', edition__speakers_viewable=True)
                .order_by('-edition__year')
                .all(),
                key=lambda e: (e.edition.id, e.edition.title),
            )
        }

        if not self.request.user.is_anonymous:
            favorites = self.request.user.favorites.all()
            ctx['is_favorite'] = {event: event in favorites for event in self.object.events.all()}
            attending = self.request.user.attending.all()
            ctx['is_going'] = {event: event in attending for event in self.object.events.all()}

        return {**ctx, **{'events_per_edition': events_per_edition}}

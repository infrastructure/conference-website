def is_attending_edition(user, edition) -> bool:
    if not user or user.is_anonymous:
        return False
    if not edition:
        return False
    return user.tickets.filter(edition=edition).exists()

window.CheckoutUtils = (function() {
  const apiUriStaging = 'https://staging.shop.blender.org/graphql/';
  const apiUriProduction = 'https://shop.blender.org/graphql/';
  const domainProduction = 'conference.blender.org';

  function CheckoutUtils() {
    const errorMessageEl = document.getElementById('error-message');
    const errorEl = document.getElementById('error');
    const reTicketsHome = new RegExp('/tickets/?$');

    function deleteCookie(cookieName) {
      document.cookie = cookieName + ';max-age=0';
    }

    function displayError(msg) {
      errorMessageEl.innerHTML = msg;
      errorEl.classList.remove('d-none');
    }

    function displayFieldValidationError(form, err) {
      const fieldEl = form.elements[err.field];
      if (!fieldEl) return;
      const errorEls = fieldEl.parentElement.getElementsByClassName(
          'field-validation-error'
      );
      if (!errorEls.length) return;
      const errorEl = errorEls[0];
      errorEl.textContent = err.message;
      errorEl.classList.remove('d-none');
    }

    function clearErrors(form) {
      errorMessageEl.textContent = '';
      errorEl.classList.add('d-none');

      if (!form) return;
      const fieldErrorEls = form.getElementsByClassName(
          'field-validation-error'
      );
      for (let i = 0; i < fieldErrorEls.length; i++) {
        fieldErrorEls[i].classList.add('d-none');
      }
    }

    function formatMoney(data) {
      // Dinero expects amounts in minor units (cents)
      const intAmount = parseInt(data.amount * 100);
      const money = {amount: intAmount, currency: data.currency};
      // eslint-disable-next-line new-cap
      return Dinero(money).toFormat('$0.00');
    }

    function getClient() {
      const apiUri = (window.location.host === domainProduction) ? apiUriProduction : apiUriStaging;
      const networkInterface = Apollo.lib.createNetworkInterface({uri: apiUri});
      // N.B.: this version of ApolloClient doesn't support setting a fetchPolicy via defaultOptions,
      // so fetchPolicy must be set in every client call later.
      // ApolloClient itself doesn't have a stable working CDN build,
      // see https://github.com/apollographql/apollo-client/issues/4595
      const client = new Apollo.lib.ApolloClient({networkInterface});
      return client;
    }

    function showEl(el) {
      el.classList.remove('d-none');
    }

    function hideEl(el) {
      el.classList.add('d-none');
    }

    function enableFieldset(fieldset) {
      const elements = fieldset.elements;
      for (let i = 0; i < elements.length; i++) {
        const el = elements[i];
        if (el.tagName.toLowerCase() === 'select') {
          el.disabled = (el.dataset.keepReadOnly) ? true : false;
        } else {
          el.readOnly = (el.dataset.keepReadOnly) ? true : false;
        }
      }
    }

    function disableFieldset(fieldset) {
      const elements = fieldset.elements;
      for (let i = 0; i < elements.length; i++) {
        const el = elements[i];
        if (el.tagName.toLowerCase() === 'select') {
          el.disabled = true;
        } else {
          el.readOnly = true;
        }
      }
    }

    function getFormFieldsValues(fieldset) {
      const formData = {};
      const elements = fieldset.elements;
      for (let i = 0; i < elements.length; i++) {
        formData[elements[i].id] = elements[i].value;
      }
      return formData;
    }

    function getLocale() {
      if (navigator.languages != undefined)
        return navigator.languages[0];
      return navigator.language;
    }

    function getCountryCode() {
      // FIXME(anna): a better way of getting a country code
      return 'NL';
      // TODO(anna): instead setup https://docs.djangoproject.com/en/3.2/ref/contrib/gis/geoip2/
      const locale = getLocale();
      const localeSplit = locale.split('-');
      return (localeSplit.length > 1) ? localeSplit[1]: null;
    }

    function getChannelSlug() {
      const countryCode = getCountryCode() || 'NL';

      const channelSlug = (countryCode == 'US') ? 'channel-usd' : 'default-channel';

      return channelSlug;
    }

    function throttleFunction(func, delay) {
      let throttlePause;

      const throttle = function() {
        // Don't run the function if throttlePause is true
        if (throttlePause) return;

        // Set throttlePause to true after the if condition. This allows the function to be run once
        throttlePause = true;

        // SetTimeout runs the func with the specified delay
        setTimeout(() => {
          func();

          // Set throttle flag to false once the function has been called, allowing the throttle function to loop
          throttlePause = false;
        }, delay);
      }
      return throttle;
    }

    function getMetadataValueByKey(metadata, key) {
      let value;
      metadata.forEach(function(kv) {
        if (kv.key === key) {
          value = kv.value;
        }
      });
      return value;
    }

    return Object.freeze({
      apiUriProduction,
      apiUriStaging,
      clearErrors,
      deleteCookie,
      disableFieldset,
      displayError,
      displayFieldValidationError,
      domainProduction,
      enableFieldset,
      formatMoney,
      getChannelSlug,
      getClient,
      getCountryCode,
      getFormFieldsValues,
      getLocale,
      getMetadataValueByKey,
      hideEl,
      reTicketsHome,
      showEl,
      throttleFunction
    });
  };
  return Object.freeze(CheckoutUtils);
})();

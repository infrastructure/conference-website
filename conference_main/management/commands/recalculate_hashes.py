import logging

from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from conference_main.models import Photo

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """Recalculate hashes of existing photos.

    This command only exists because `hash` field was added to a table with existing records,
    and is assumed to only run once.
    """

    def handle(self, *args, **options):
        """Calculate hashes of existing photos and save them."""
        for photo in Photo.objects.all():
            photo.hash = photo.generate_hash(photo.file)
            try:
                photo.save(update_fields={'hash', 'updated_at'})
            except IntegrityError as e:
                logger.error('Skipping pk=%s: %s', photo.pk, e)

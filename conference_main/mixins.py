"""Commonly used model and admin mixins."""
from functools import lru_cache
from typing import Optional
import logging

from django.conf import settings
from django.utils.safestring import mark_safe


from sorl.thumbnail import get_thumbnail

log = logging.getLogger(__name__)


@lru_cache(maxsize=1024)
def _cacheable_get_thumbnail(thumbnail, size_settings):
    return get_thumbnail(thumbnail, size_settings, crop=settings.THUMBNAIL_CROP_MODE).url


class StaticThumbnailURLMixin:
    """Add `thumbnail_<size>_url` properties generating static cacheable thumbnail URLs."""

    file = None  # Is always overridden

    def _get_thumbnail(self, size_settings):
        if not self.file:
            return None
        try:
            return _cacheable_get_thumbnail(self.file, size_settings)
        except OSError as e:
            # Handle the classic 'cannot write mode RGBA as JPEG'
            log.error(e)
            return None

    @property
    def thumbnail_m_url(self) -> Optional[str]:
        """Return a static URL to a medium-sized thumbnail."""
        return self._get_thumbnail(settings.THUMBNAIL_SIZE_M)

    @property
    def thumbnail_s_url(self) -> Optional[str]:
        """Return a static URL to a small thumbnail."""
        return self._get_thumbnail(settings.THUMBNAIL_SIZE_S)


class ThumbnailMixin:
    """Display an asset thumbnail, if available."""

    def view_thumbnail(self, obj):
        """Return an img tag with a thumbnail, if available."""
        img_url = getattr(obj, 'thumbnail_s_url', None)
        if img_url:
            return mark_safe(f'<img width=100 src="{img_url}">')
        return ''

    view_thumbnail.allow_tags = True

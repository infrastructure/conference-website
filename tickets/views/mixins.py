import logging

from django.contrib.auth.mixins import AccessMixin

logger = logging.getLogger(__name__)


class TicketClaimedOrBoughtRequiredMixin(AccessMixin):
    """Verify that current user has claimed or bought this ticket."""

    def dispatch(self, request, *args, **kwargs):
        ticket = self.get_object()
        self.attendee = None
        is_claimed_by = ticket.is_claimed_by(user_id=request.user.pk)
        if ticket.user_id == request.user.pk or is_claimed_by:
            if is_claimed_by:
                self.attendee = request.user
            return super().dispatch(request, *args, **kwargs)
        logger.warning(
            'User %s is attempting to access ticket %s, but they have no access to it',
            request.user.pk,
            ticket.pk,
        )
        return self.handle_no_permission()


class TicketBoughtRequiredMixin(AccessMixin):
    """Verify that current user has bought this ticket."""

    def dispatch(self, request, *args, **kwargs):
        ticket = self.get_object()
        if ticket.user_id == request.user.pk or request.user.has_perm('tickets.view_ticket'):
            return super().dispatch(request, *args, **kwargs)
        logger.warning(
            'User %s is attempting to access ticket %s, but they have no access to it',
            request.user.pk,
            ticket.pk,
        )
        return self.handle_no_permission()

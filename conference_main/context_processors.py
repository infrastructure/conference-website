import random
from typing import Mapping, Optional

from django.conf import settings
from django.contrib.sites.models import Site
from django.http import HttpRequest

from conference_main.models import Edition
from tickets.queries import is_attending_edition


def edition_and_site_settings(request: HttpRequest) -> Mapping[str, object]:
    """
    Inject the SiteSettings into the template context.
    Inject the Edition as identified by the `edition_path` argument in urls.py
    into the template context, as well as whether the user is attending or not.
    """

    context = {
        'site_settings': None,
        'edition': None,
        'edition_sponsors': None,
        'user_is_attending_edition': None,
        'DEFAULT_REPLY_TO_EMAIL': settings.DEFAULT_REPLY_TO_EMAIL,
    }

    site = Site.objects.get_current(request)

    if hasattr(site, 'settings'):
        # The default Django site has no settings, so this should only happen on a new install.
        context['site_settings'] = site.settings
        context['edition'] = site.settings.current_edition

    edition_sponsors = {}
    if request.resolver_match is not None and 'edition_path' in request.resolver_match.kwargs:
        edition = Edition.objects.get(path=request.resolver_match.kwargs['edition_path'])
        for sponsor in edition.sponsors.all():
            edition_sponsors.setdefault(sponsor.level.name, []).append(sponsor)

        # Randomize sponsors order within the same level, for fairness
        for _, sponsors in edition_sponsors.items():
            random.shuffle(sponsors)

        context['edition'] = edition
        context['edition_sponsors'] = edition_sponsors
        context['user_is_attending_edition'] = is_attending_edition(request.user, edition)

    return context


def main_menu_editions(request: HttpRequest) -> Mapping[str, object]:
    """
    Inject the Editions which have `show_in_main_menu == True`.
    """
    return {'main_menu_editions': Edition.objects.filter(show_in_main_menu=True).order_by('-year')}
